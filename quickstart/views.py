import os
from io import BytesIO

import keras
import tensorflow as tf
from PIL import Image
from rest_framework.response import Response
from rest_framework.views import APIView
import numpy as np

from quickstart.serializer import UploadSerializer
from quickstart.utils import predict_to_image_response


class UploadView(APIView):
    serializer_class = UploadSerializer
    export_path = os.path.join(os.getcwd(), 'quickstart', 'model.h5') #model path
    model = tf.keras.models.load_model(export_path, compile=True) #modeli yüklüyoruz

    def post(self, request):
        fileImg = request.FILES.get('file_img') #body deki imageyi yakala
        image = Image.open(BytesIO(fileImg.file.getvalue())).convert('1').resize((56, 56)) #grayscale dondurme ve resize ettik
        x = keras.preprocessing.image.img_to_array(image) #img to array
        x = x.reshape(56, 56, 1).astype('float')
        x = np.expand_dims(x, axis=-1)

        predictions = self.model.predict(x[None,:,:]) #predict ettiğimiz kısım

        return Response(predict_to_image_response(predictions))
